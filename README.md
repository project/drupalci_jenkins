DrupalCI jenkins
================

### Overview

Provides Jenknins master/slave configuration for DrupalCI testbots.

### Documentation

* [Setup](/docs/setup.md) - _Installing packages for local development._
* [Local development](/docs/local.md) - _Performing local development._
* [Packaging](/docs/packer.md) - _Packaging and how we handle this for multiple services / technologies._
