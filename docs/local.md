Local development
=================

A local environment can be setup with Vagrant using the following command:

```
$ vagrant up
```

Note: Go get a coffee, this might take a little while.

Go to the following URL:

```
http://master.local
```

##### Manual method

If you wish to run some manual steps please see for a example of the commands you can run:

```
$ cd puppet
$ ./scripts/provision.sh
```
